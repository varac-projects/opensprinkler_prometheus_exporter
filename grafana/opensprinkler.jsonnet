# Todo:
#
#   * Fix date singlestat for last weather API call which shows the wrong time
#     The grafonnet-lib API doesn't provide a `fields` key for `singlestat`, but
#     it does so for `stat`

local grafana = import 'github.com/grafana/grafonnet-lib/grafonnet/grafana.libsonnet';
local dashboard = grafana.dashboard;
local row = grafana.row;
local prometheus = grafana.prometheus;
local template = grafana.template;
local graphPanel = grafana.graphPanel;
local singlestat = grafana.singlestat;

local precipation =
  graphPanel.new(
    'Precipation',
    datasource='Prometheus',
    format='mm',
    decimals=1,
  )
  .addTarget(prometheus.target('opensprinkler_wt_precip',
      legendFormat='precipation')
  );

local evapotranspiration =
  graphPanel.new(
    'Evapotranspiration (ETo)',
    datasource='Prometheus',
    # format='',
    decimals=1,
  )
  .addTarget(prometheus.target('opensprinkler_wt_eto',
      legendFormat='{{ type }}')
  );

local radiation =
  graphPanel.new(
    'Solar radiation',
    datasource='Prometheus',
    format='Wm2',
    decimals=1,
  )
  .addTarget(prometheus.target('opensprinkler_wt_radiation',
      legendFormat='Solar radiation')
  );

local temperature =
  graphPanel.new(
    'Temperature',
    datasource='Prometheus',
    format='celsius',
    decimals=1,
  )
  .addTarget(prometheus.target('opensprinkler_wt_temp_c',
      legendFormat='{{ type }}')
  );

local humidity =
  graphPanel.new(
    'Humidity',
    datasource='Prometheus',
    format='percent',
    decimals=1,
    max=100
  )
  .addTarget(prometheus.target('opensprinkler_wt_hum',
      legendFormat='{{ type }}')
  );

local wind =
  graphPanel.new(
    'Wind',
    datasource='Prometheus',
    format='velocitymph',
    decimals=1,
  )
  .addTarget(prometheus.target('opensprinkler_wt_wind',
      legendFormat='Wind velocity')
  );

local water_level =
  graphPanel.new(
    'Water level',
    datasource='Prometheus',
    format='percent',
    decimals=1,
    max=250
  )
  .addTarget(prometheus.target('opensprinkler_water_level',
      legendFormat='Calculated water level')
  );

local station_open =
  graphPanel.new(
    'Station open',
    datasource='Prometheus',
    max=1
  )
  .addTarget(prometheus.target('opensprinkler_station_open',
      legendFormat='{{ station }}')
  );

local last_request =
  singlestat.new(
    'Last weather provider request',
    datasource='Prometheus',
    format='dateTimeAsLocal'
  )
  .addTarget(prometheus.target('opensprinkler_last_weather_call{type="request"}',
      legendFormat='{{ type }}')
  );

dashboard.new(
  'Opensprinkler',
  time_from='now-1d',
  editable=true,
)
.addRow(
  row.new()
  .addPanel(evapotranspiration)
  .addPanel(precipation)
  .addPanel(wind)
)
.addRow(
  row.new()
  .addPanel(radiation)
  .addPanel(temperature)
  .addPanel(humidity)
)
.addRow(
  row.new()
  .addPanel(water_level)
  .addPanel(station_open)
)
.addRow(
  row.new()
  .addPanel(last_request)
)
