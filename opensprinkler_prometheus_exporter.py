#!/usr/bin/env python3
"""Prometheus exporter for opensprinkler metrics.

Exports Opensprinkler metrics that can get scraped by Prometheus.

See https://openthings.freshdesk.com/support/solutions/articles/5000716363-os-api-documents
for Opensprinkler API documentation and https://github.com/OpenSprinkler/OpenSprinkler-Weather
for the weather provider API (I couldn't find any weather provider API docs)

Author: varac@varac.net

Todo:
  * Add celsius temp values
  * Add units of measurement
"""

import json
import logging
import os
import time
# from datetime import datetime

import plac
import requests
from prometheus_client import start_http_server, Gauge

logging.basicConfig()
log = logging.getLogger(__name__)
loglevel = os.environ.get('LOGLEVEL', 'WARNING').upper()
log.setLevel(level=loglevel)


# Initialize Prometheus instrumentation
wt_eto = Gauge('opensprinkler_wt_eto',
               'Weather provider: Evapotranspiration (ETo)', ['type'])
wt_radiation = Gauge('opensprinkler_wt_radiation',
                     'Weather provider: Solar radiation, W/m²')
wt_temp_f = Gauge('opensprinkler_wt_temp_f',
                  'Weather provider: Temperature (min/max), Fahrenheid', ['type'])
wt_temp_c = Gauge('opensprinkler_wt_temp_c',
                  'Weather provider: Temperature (min/max), Celsius', ['type'])
wt_hum = Gauge('opensprinkler_wt_hum',
               'Weather provider: Humidity (min/max), %', ['type'])
wt_wind = Gauge('opensprinkler_wt_wind', 'Weather provider: Wind speed, m/h')
wt_precip = Gauge('opensprinkler_wt_precip',
                  'Weather provider: Precipation, mm')
sun = Gauge('opensprinkler_sun',
            'Daytime of sunrise (sunrise/sunset), mins', ['type'])
last_weather_call = Gauge('opensprinkler_last_weather_call',
                          'Timestamp of the last weather call/query (request/response), epoch time',
                          ['type'])
last_weather_call_err = Gauge('opensprinkler_last_weather_call_err',
                              'Error code of last weather call, int')
water_level = Gauge('opensprinkler_water_level',
                    'Water level (0 to 250), %')
station_open = Gauge('opensprinkler_station_open',
                     'Station status, 0=closed, 1=open', ['station'])


def main():
    """Start main function."""
    # Setup logging
    loglevel = getattr(logging, os.environ.get('LOGLEVEL', 'INFO').upper())
    log.setLevel(level=loglevel)

    interval = int(os.environ.get('INTERVAL', 60))

    password = str(os.environ['OS_API_PW'])
    api_url = str(os.environ.get('OS_API_URL', 'http://localhost:8080'))
    url = f'{api_url}/ja?pw={password}'
    port = int(os.environ.get('PORT', 3089))
    log.info('Starting prometheus exporter on port %s', str(port))
    log.debug('Using URL: %s', url)
    start_http_server(port)

    while True:
        try:
            req = requests.get(url)

            if req.status_code == 200:
                j = json.loads(req.content)
                log.debug('Got a successful response: %s', j)
                wto = j['settings']['wto']
                wt_eto.labels(type='base').set(wto['baseETo'])
                wterr = j['settings']['wterr']

                wtdata = j['settings']['wtdata']
                if wtdata:
                    wt_eto.labels(type='current').set(wtdata['eto'])
                    wt_radiation.set(wtdata['radiation'])
                    wt_temp_f.labels(type='min').set(wtdata['minT'])
                    wt_temp_f.labels(type='max').set(wtdata['maxT'])
                    wt_temp_c.labels(type='min').set(
                        (wtdata['minT'] - 32) * 5.0/9.0)
                    wt_temp_c.labels(type='max').set(
                        (wtdata['maxT'] - 32) * 5.0/9.0)
                    wt_hum.labels(type='min').set(wtdata['minH'])
                    wt_hum.labels(type='max').set(wtdata['maxH'])
                    wt_wind.set(wtdata['wind'])
                    wt_precip.set(wtdata['p'])
                else:
                    log.warning('Opensprinkler couldnt get weather provider data, error code: {}'
                                .format(wterr))

                sun.labels(type='sunrise').set(j['settings']['sunrise'])
                sun.labels(type='sunset').set(j['settings']['sunset'])

                last_weather_call_err.set(wterr)
                last_weather_call.labels(
                    type='request').set(j['settings']['lwc'])
                last_weather_call.labels(type='response').set(
                    j['settings']['lswc'])

                water_level.set(j['options']['wl'])

                station_status = j['status']['sn']
                for i in range(j['status']['nstations']):
                    station_open.labels(station=i+1).set(station_status[i])

            else:

                log.info(req.status_code)
                log.info(req.content)

        except KeyboardInterrupt:
            break

        log.debug('sleeping for %d seconds', interval)
        time.sleep(interval)


if __name__ == '__main__':
    plac.call(main)
