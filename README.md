# Prometheus exporter for Opensprinkler metrics

It queries the Opensprinkler API and exports Opensprinkler metrics that can get scraped by Prometheus.
Example debugging call:

    curl "http://opensprinkler.lan:8080/ja?pw=..." | jq .


## Usage

Export required env vars:

    export OS_API_URL='http://opensprinkler.lan:8080'
    export OS_API_PW='...'

Optionally increase the [log level](https://docs.python.org/3/library/logging.html#logging-levels):

    export LOGLEVEL=DEBUG

Run:

    ./opensprinkler_prometheus_exporter.py

Query:

    curl localhost:3089


## Systemd service

See `opensprinkler_prometheus_exporter.service` for an example system service
unit file.

## Grafana dashbord

See `grafana/opensprinkler.json` for an example dashboard
and `grafana/opensprinkler.jsonnet` for it's
[grafonnet](https://grafana.github.io/grafonnet-lib/)/jsonnet code.

![dashboard.png](./grafana/dashboard.png)

## References

* [Opensprinkler](https://opensprinkler.com/)
* [Opensprinkler API documentation](https://openthings.freshdesk.com/support/solutions/articles/5000716363-os-api-documents)
* See [OpenSprinkler-Weather](https://github.com/OpenSprinkler/OpenSprinkler-Weather)
  for the weather provider API (I couldn't find any weather provider API docs),
  i.e. in [routes/weatherProviders/local.ts#L24](https://github.com/OpenSprinkler/OpenSprinkler-Weather/blob/master/routes/weatherProviders/local.ts#L24)

Author: varac@varac.net
